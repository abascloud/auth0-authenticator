# \<auth0-authenticator\>

You should use the \<auth0-authenticator\> in combination with \<abas-config\>.

```html
<abas-config id="config" data="{{configuration}}"></abas-config>
<auth0-authenticator id="auth0" 
    tenant="[[configuration.meta.auth0tenant]]" 
    connection="[[configuration.meta.tenant]]" 
    client="[[configuration.clients.yourApplication]]" 
    redirect 
    profile="{{profile}}" 
    decoded-token="{{decodedToken}}">
</auth0-authenticator>
```

When loading your website, by setting the query parameter `adminManagement` to `true` in the url. Your application will redirect to the auth0 federation account so that the manager of the tenant could login.
To change the query parameter name specify the `admin-param-name`

The redirect property allow you to be redirected on the original page that landed into without loosing information about `hashtags`
# \<auth0-ajax\>

Works exactly like an \<iron-ajax\>, it will just automatically populate the `Authorization` header field for you. 

# \<auth0-websocket\>
In progress...
# auth0Fetch

You can use this like you would use ```fetch```.
[Here](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API) you can find a good Documentation on how to use auth0Fetch. Just replace ```fetch``` with ```auth0Fetch```.

```
let headers = new Headers();
headers.set('accept', 'application/abas.objects+json');
auth0Fetch('https://erp.test.us-east-1.api.abas.ninja/mw/r/', {
    headers: headers
}).then((resp) => {
    if(resp.status === 200) return resp.json();
}).then((data) => {
    //do something with data
});
```

Please keep in mind that auth0Fetch does not take care of login. If there is no token it will throw an exception.